<?php

	namespace AppBundle\Lib\Solarium\Plugin;

	use Solarium\Core\Plugin\AbstractPlugin;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;


	class Autocompletion extends AbstractPlugin
	{

			private $suggestQuery;

				/**
	     * Plugin init function.
	     *
	     * This is an extension point for plugin implementations.
     	 * Will be called as soon as $this->client and options have been set.
	     */
	    protected function initPluginType()
	    {
	    		$controller = new Controller();
	    		$this->client = $controller->get('solarium.client');
	        $this->suggestQuery = $this->client->createSuggester();
	    }

			public function suggetion ($keyword)
			{
					$this->suggestQuery->setQuery($keyword);
					$this->suggestQuery->setDictionary('suggest');
					$this->suggestQuery->setCount(10);

					return json_encode( $this->client->suggester( $this->suggestQuery) );
			}

	}