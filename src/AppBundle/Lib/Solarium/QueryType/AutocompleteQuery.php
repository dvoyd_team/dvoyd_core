<?php

	namespace AppBundle\Lib\Solarium\QueryType;

	use Solarium\Client;
	use Solarium\QueryType\Suggester\Query as Suggester;

	class AutocompleteQuery extends suggester
	{
			protected function init()
			{
					$this->setDictionary('dvoydsuggester');
					$this->setOnlyMorePopular(true);
					$this->setCount(4);
					$this->setCollate(true);
			}	
	}