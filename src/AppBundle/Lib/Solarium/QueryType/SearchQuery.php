<?php

	namespace AppBundle\Lib\Solarium\QueryType;

	use Solarium\Client;
	use Solarium\QueryType\Select\Query\Query as Select;

	class SearchQuery extends Select
	{
			protected function init()
			{
					parent::init();
					$edismax = $this->getEDisMax();
					$edismax->setMinimumMatch(1);
					$edismax->setPhraseSlop(1);
					$edismax->setQueryFields('term');
					$this->setFields(array('name','image','description','domaine','link','price','categories'));
					$this->getFacetSet()->createFacetField('domaineFacet')->setField('domaine');
					$this->setRows('12');
					
					/*
					$spellcheck = $this->getSpellcheck();
					$spellcheck->setCount(1);
					$spellcheck->setBuild(true);
					$spellcheck->setCollate(true);
					$spellcheck->setExtendedResults(true);
					$spellcheck->setCollateExtendedResults(true);
					
					$mlt = $this->getMoreLikeThis();
			      $mlt->setFields('name');
			      $mlt->setMinimumDocumentFrequency(1);
			      $mlt->setMinimumTermFrequency(1); 
			      */
			}	
	}