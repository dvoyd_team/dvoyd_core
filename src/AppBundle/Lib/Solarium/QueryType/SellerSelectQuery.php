<?php 

	namespace AppBundle\Lib\Solarium\QueryType;

	class PriceIntervalQuery extends PriceQuery
	{

		public function __construct($min, $max)
		{
			$this->_minprice = $min;
			$this->_maxprice = $max;
			$this->createFilterQuery('price_range')->setQuery('price:[%1% TO %2%]',
				array($this->_minprice, $this->_maxprice))
			;
		}

	}