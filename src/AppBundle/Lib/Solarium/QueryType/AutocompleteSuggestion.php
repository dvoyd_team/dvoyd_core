<?php

	namespace AppBundle\Lib\Solarium\QueryType;


	class AutocompleteSuggestion extends AutocompleteQuery
	{

		public function __construct($terms)
		{
			parent::init();
			$this->setQuery($terms);
		}

	}