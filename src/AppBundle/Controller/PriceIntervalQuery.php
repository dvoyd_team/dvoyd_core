<?php 

	namespace AppBundle\Lib\Solarium\QueryType;

	use Solarium\QueryType\Select\Query\Query as Select;

	class PriceIntervalQuery extends PriceQuery
	{

		public function __construct($min, $max)
		{
			$this->_minprice = $min;
			$this->_maxprice = $max;
			$this->createFilterQuery->setQuery('price:[%1% TO %2%]',
			array($this->_minprice, $this->_maxprice));
		}

	}