<?php

   namespace AppBundle\Controller;

   use AppBundle\Controller\Api\HesRestController;
   use AppBundle\Lib\Solarium\QueryType\SearchQuery;
   use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

   class HesResultController extends HesRestController
   {

      /**
      *@Route("/{_locale}/results", name="search_result", defaults={"_locale": "fr"}, requirements={"_locale": "fr|en"})
      *
      */
      public function showAction($_locale)
      {  
        $client = $this->get('solarium.client');
        $request = $this->getRequest();
        
      }

      public function menuAction($q)
      {
        return $this->render('AppBundle:DvoydSearch:sidenav-menu.html.twig', array('q' => $q));
      }
      
   }
