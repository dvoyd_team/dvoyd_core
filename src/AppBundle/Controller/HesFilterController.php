<?php

	namespace AppBundle\Controller;

	use AppBundle\Controller\Api\HesRestController;
   use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
   use Symfony\Component\HttpFoundation\Request;
   use AppBundle\Lib\Solarium\QueryType\PriceIntervalQuery;

  class HesFilterController extends HesRestController
  {
    /**
     *
     *@Route("/{_locale}/filter/price", name="filter_price", defaults={"_locale": "fr"}, 
     *requirements={"_locale": "fr|en"})
     */
  	public function priceIntervalAction(Request $request,  $_locale)
  	{
      $client = $this->get('solarium.client');

      $q = $request->query->get('q');
      $min = $request->query->get('minp'); 
      $max = $request->query->get('maxp');
      $p = $request->query->get('p',1);
      $start = ($p - 1) * 12;

      $Squery = new PriceIntervalQuery( $min, $max );

      $Squery->setQuery('%1%^2.5 *%1%*^2.0 *%1%^2.0 %1%*^2.0',array($q) );
      $Squery->setStart($start);
      $resultset = $client->execute($Squery);
      $nf = $resultset->getNumfound();

      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(array($client,$Squery), $p, 12);

  	  if ( $request->isXmlHttpRequest())
      {

         return $this->render("AppBundle:DvoydSearch:show_result.html.twig",
         array('q' => $q,'nf' => $nf , 'results' => $resultset ,'pagination' => $pagination)) ;
      }else{

         return $this->render("AppBundle:DvoydSearch:show_resultat.html.twig", array('q' => $q,
         'nf' => $nf , 'results' => $resultset ,'pagination' => $pagination ));
      }
   }


    /**
     *
     *@Route("/{_locale}/filter/seller", name="filter_seller", defaults={"_locale": "fr"}, 
     *requirements={"_locale": "fr|en"})
     */
     public function sellerSelectAction()
     {
       $client = $this->get('solarium.client');

      $q = $request->query->get('q');
      $min = $request->query->get('minp'); 
      $max = $request->query->get('maxp');
      $p = $request->query->get('p',1);
      $start = ($p - 1) * 12;

      $Squery = new PriceIntervalQuery( $min, $max );

      $Squery->setQuery('%1%^2.5 *%1%*^2.0 *%1%^2.0 %1%*^2.0',array($q) );
      $Squery->setStart($start);
      $resultset = $client->execute($Squery);
      $nf = $resultset->getNumfound();

      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(array($client,$Squery), $p, 12);

      if ( $request->isXmlHttpRequest())
      {

         return $this->render("AppBundle:DvoydSearch:show_result.html.twig",
         array('q' => $q,'nf' => $nf , 'results' => $resultset ,'pagination' => $pagination)) ;
      }else{

         return $this->render("AppBundle:DvoydSearch:show_resultat.html.twig", array('q' => $q,
         'nf' => $nf , 'results' => $resultset ,'pagination' => $pagination ));
      }
     }
  }