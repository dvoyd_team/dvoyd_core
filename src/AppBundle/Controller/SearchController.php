<?php

  namespace AppBundle\Controller;

  use AppBundle\Controller\Api\HesRestController;
  use AppBundle\Lib\Solarium\QueryType\PriceIntervalQuery;
  use AppBundle\Lib\Solarium\QueryType\SearchQuery;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\HttpFoundation\Request;

   class SearchController extends HesRestController
   {

      /**
      *
      *@Route("/{_locale}/search/", name="search_query", defaults={"_locale": "fr"}, 
      *requirements={"_locale": "fr|en"})
      */
      public function searchAction(Request $request, $_locale)
      {
        $client = $this->get('solarium.client');

        $q = $request->query->get('q');
        $p = $request->query->get('p',1);
        $start = ($p - 1) * 12;

        if( $request->query->get('minp') && $request->query->get('maxp') )
        {
            $Squery = new PriceIntervalQuery( $request->query->get('minp')
               , $request->query->get('maxp') );
        }else{
            $Squery = new SearchQuery();
        }

        $Squery->setQuery($q);
        $Squery->setStart($start);
        $resultset = $client->execute($Squery);
        $nf = $resultset->getNumfound();
        $domaineFacet = $resultset->getFacetSet()->getFacet('domaineFacet');

        /**
        *
        * KNPPaginator Object
        * Create a KnpPaginator Object with the param
        *
        **/
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(array($client,$Squery), $p, 12);

        if ($request->isXmlHttpRequest() )
        {
          return $this->render("AppBundle:DvoydSearch:show_result.html.twig",
           array('q' => $q,'nf' => $nf , 'results' => $resultset, 'domaineFacet' => $domaineFacet,
            'pagination' => $pagination)) ;
        }else{

         return $this->render("AppBundle:DvoydSearch:show_resultat.html.twig", 
          array('q' => $q,'nf' => $nf , 'results' => $resultset , 'domaineFacet' => $domaineFacet,
            'pagination' => $pagination));
        }
      }

      public function menuAction()
      {
        return $this->render('AppBundle:DvoydSearch:sidenav-menu.html.twig');
      }

      public function mobilenavAction()
      {
        return $this->render('AppBundle:DvoydSearch:mobile_nav.html.twig');
      }
   }
