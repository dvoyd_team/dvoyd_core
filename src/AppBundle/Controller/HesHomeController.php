<?php
  
  namespace AppBundle\Controller;

  use AppBundle\Controller\Api\HesRestController;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\HttpFoundation\Request;

  Class HesHomeController extends HesRestController
  {
      
      /**
       *
       *@Route("/{_locale}", name="frontend_homepage", defaults={"_locale": "fr"}, 
       *requirements={"_locale": "fr|en"})
       */
      public function indexAction(Request $request)
      {
        $view = $this->view()->setTemplate('AppBundle:DvoydSearch:home_page.html.twig');
        return $this->handleView($view);
      }

  }
