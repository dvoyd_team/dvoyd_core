<?php

  namespace AppBundle\Controller;

  use AppBundle\Lib\Solarium\QueryType\AutocompleteSuggestion;
  use AppBundle\Controller\Api\HesRestController;
  use FOS\RestBundle\Request\ParamFetcher;
  use FOS\RestBundle\Controller\Annotations\RequestParam;
  use FOS\RestBundle\Controller\Annotations\QueryParam;
  use FOS\RestBundle\Controller\Annotations\FileParam;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

  class AutocompletionController extends HesRestController
  {
      /**
       *
       *
       *@Route("/{_locale}/search/autocomplete/suggest" , name="autocomplete" ,defaults={"_locale": "fr"}, 
       *requirements={"_locale": "fr|en"}))
       */
      public function suggestAction()
      {
          $request = $this->getRequest();
        $q = $request->query->get('q');
        $client = $this->get('solarium.client');

        $autocompleteSuggest = new AutocompleteSuggestion($q);
        $suggests = $client->suggester($autocompleteSuggest);

        return new json_encode($suggests);
      }
  }