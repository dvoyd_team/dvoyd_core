<?php

    namespace AppBundle\Decoder;

    use FOS\RestBundle\Decoder\DecoderInterface;

    /**
    * Decodes JSON data.
    *
    * @author RedHack Anonymous <redhackmaster@gmail.com>
    */
    class HesJsonDecoder implements DecoderInterface
    {
        /**
        * {@inheritdoc}
        */
        public function decode($data)
        {
            return @json_decode($data, true);
        }
    }
