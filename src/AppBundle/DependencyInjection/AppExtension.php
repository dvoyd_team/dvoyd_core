<?php

    namespace AppBundle\DependencyInjection;

    use Symfony\Component\HttpKernel\DependencyInjection\Extension;
    use Symfony\Component\DependencyInjection\ContainerBuilder;
    use Symfony\Component\DependencyInjection\Loader;
    use Symfony\Component\Config\FileLocator;

    class AppExtension extends Extension
    {
        public function load(array $configs, ContainerBuilder $container) {
            // ...
            $loader1 = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
            $loader2 = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
            // ...
            $loader1->load('admin.yml');
            $loader2->load('hes_body_listener.xml','hes_decoder_json');
        }
    }
