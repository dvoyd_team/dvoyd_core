<?php

	namespace AppBundle\Twig;

	class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('json_decode',array($this, 'json_decode')),
            new \Twig_SimpleFilter('format_name',array($this, 'format_name')),
        );
    }

    public function json_decode($data)
    {
        $data = json_decode($data);
        return $data;
    }

    public function format_name($name)
    {

        $name = preg_replace("#(w*\.)?(.+)\.[a-z]{2,3}#", "$2", $name) ;
        return $name;
    }

    public function getName()
    {
        return 'app_extension';
    }
}