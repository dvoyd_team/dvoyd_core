<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb82f97cdf82207d6cccfcbd6127982e6
{
    public static $files = array (
        '92c8763cd6170fce6fcfe7e26b4e8c10' => __DIR__ . '/..' . '/symfony/phpunit-bridge/bootstrap.php',
        'c964ee0ededf28c96ebd9db5099ef910' => __DIR__ . '/..' . '/guzzlehttp/promises/src/functions_include.php',
        'a0edc8309cc5e1d60e3047b5df6b7052' => __DIR__ . '/..' . '/guzzlehttp/psr7/src/functions_include.php',
        '37a3dc5111fe8f707ab4c132ef1dbc62' => __DIR__ . '/..' . '/guzzlehttp/guzzle/src/functions_include.php',
        '5255c38a0faeba867671b61dfda6d864' => __DIR__ . '/..' . '/paragonie/random_compat/lib/random.php',
        'e40631d46120a9c38ea139981f8dab26' => __DIR__ . '/..' . '/ircmaxell/password-compat/lib/password.php',
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        '023d27dca8066ef29e6739335ea73bad' => __DIR__ . '/..' . '/symfony/polyfill-php70/bootstrap.php',
        'bd9634f2d41831496de0d3dfe4c94881' => __DIR__ . '/..' . '/symfony/polyfill-php56/bootstrap.php',
        'edc6464955a37aa4d5fbf39d40fb6ee7' => __DIR__ . '/..' . '/symfony/polyfill-php55/bootstrap.php',
        '3e2471375464aac821502deb0ac64275' => __DIR__ . '/..' . '/symfony/polyfill-php54/bootstrap.php',
        '6a47392539ca2329373e0d33e1dba053' => __DIR__ . '/..' . '/symfony/polyfill-intl-icu/bootstrap.php',
        '32dcc8afd4335739640db7d200c1971d' => __DIR__ . '/..' . '/symfony/polyfill-apcu/bootstrap.php',
        '8ac9dba77d5c65db2811440ee8265f5d' => __DIR__ . '/..' . '/sonata-project/block-bundle/Resources/stubs/symfony2.php',
        'ce89ac35a6c330c55f4710717db9ff78' => __DIR__ . '/..' . '/kriswallsmith/assetic/src/functions.php',
    );

    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Polyfill\\Util\\' => 22,
            'Symfony\\Polyfill\\Php70\\' => 23,
            'Symfony\\Polyfill\\Php56\\' => 23,
            'Symfony\\Polyfill\\Php55\\' => 23,
            'Symfony\\Polyfill\\Php54\\' => 23,
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Component\\Security\\Acl\\' => 31,
            'Symfony\\Component\\ExpressionLanguage\\' => 37,
            'Symfony\\Component\\Cache\\' => 24,
            'Symfony\\Component\\' => 18,
            'Symfony\\Bundle\\MonologBundle\\' => 29,
            'Symfony\\Bundle\\AsseticBundle\\' => 29,
            'Symfony\\Bundle\\' => 15,
            'Symfony\\Bridge\\Twig\\' => 20,
            'Symfony\\Bridge\\Swiftmailer\\' => 27,
            'Symfony\\Bridge\\ProxyManager\\' => 28,
            'Symfony\\Bridge\\PhpUnit\\' => 23,
            'Symfony\\Bridge\\Monolog\\' => 23,
            'Symfony\\Bridge\\Doctrine\\' => 24,
            'Sonata\\DoctrineORMAdminBundle\\' => 30,
            'Sonata\\DatagridBundle\\' => 22,
            'Sonata\\CoreBundle\\' => 18,
            'Sonata\\Cache\\Tests\\' => 19,
            'Sonata\\Cache\\' => 13,
            'Sonata\\BlockBundle\\' => 19,
            'Sonata\\AdminBundle\\' => 19,
            'Sensio\\Bundle\\GeneratorBundle\\' => 30,
            'Sensio\\Bundle\\FrameworkExtraBundle\\' => 35,
            'Sensio\\Bundle\\DistributionBundle\\' => 33,
        ),
        'R' => 
        array (
            'Ramsey\\Uuid\\' => 12,
        ),
        'P' => 
        array (
            'Psr\\Log\\' => 8,
            'Psr\\Http\\Message\\' => 17,
            'Psr\\Cache\\' => 10,
        ),
        'O' => 
        array (
            'Oneup\\FlysystemBundle\\' => 22,
        ),
        'N' => 
        array (
            'Nelmio\\SolariumBundle\\' => 22,
            'Negotiation\\' => 12,
        ),
        'M' => 
        array (
            'Monolog\\' => 8,
            'Minimalcode\\Search\\' => 19,
        ),
        'L' => 
        array (
            'League\\Flysystem\\' => 17,
        ),
        'K' => 
        array (
            'Knp\\Menu\\' => 9,
            'Knp\\Bundle\\PaginatorBundle\\' => 27,
            'Knp\\Bundle\\MenuBundle\\' => 22,
            'Knp\\Bundle\\GaufretteBundle\\' => 27,
        ),
        'I' => 
        array (
            'Incenteev\\ParameterHandler\\' => 27,
        ),
        'H' => 
        array (
            'Http\\Promise\\' => 13,
            'Http\\Client\\' => 12,
            'Http\\Adapter\\Guzzle6\\' => 21,
        ),
        'G' => 
        array (
            'GuzzleHttp\\Psr7\\' => 16,
            'GuzzleHttp\\Promise\\' => 19,
            'GuzzleHttp\\' => 11,
        ),
        'F' => 
        array (
            'FOS\\RestBundle\\' => 15,
            'FOS\\HttpCache\\Tests\\' => 20,
            'FOS\\HttpCache\\' => 14,
            'FOS\\HttpCacheBundle\\' => 20,
            'FOS\\ElasticaBundle\\' => 19,
        ),
        'E' => 
        array (
            'Exporter\\' => 9,
            'Elastica\\' => 9,
        ),
        'D' => 
        array (
            'Doctrine\\Instantiator\\' => 22,
            'Doctrine\\Common\\Cache\\' => 22,
            'Doctrine\\Common\\Annotations\\' => 28,
            'Doctrine\\Common\\' => 16,
            'Doctrine\\Bundle\\DoctrineCacheBundle\\' => 36,
            'Doctrine\\Bundle\\DoctrineBundle\\' => 31,
        ),
        'C' => 
        array (
            'Craue\\FormFlowBundle\\' => 21,
            'Cocur\\Slugify\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Polyfill\\Util\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-util',
        ),
        'Symfony\\Polyfill\\Php70\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php70',
        ),
        'Symfony\\Polyfill\\Php56\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php56',
        ),
        'Symfony\\Polyfill\\Php55\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php55',
        ),
        'Symfony\\Polyfill\\Php54\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php54',
        ),
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Component\\Security\\Acl\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/security-acl',
        ),
        'Symfony\\Component\\ExpressionLanguage\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/expression-language',
        ),
        'Symfony\\Component\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/cache',
        ),
        'Symfony\\Component\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component',
        ),
        'Symfony\\Bundle\\MonologBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/monolog-bundle',
        ),
        'Symfony\\Bundle\\AsseticBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/assetic-bundle',
        ),
        'Symfony\\Bundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bundle',
        ),
        'Symfony\\Bridge\\Twig\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/Twig',
        ),
        'Symfony\\Bridge\\Swiftmailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/Swiftmailer',
        ),
        'Symfony\\Bridge\\ProxyManager\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/ProxyManager',
        ),
        'Symfony\\Bridge\\PhpUnit\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/phpunit-bridge',
        ),
        'Symfony\\Bridge\\Monolog\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/Monolog',
        ),
        'Symfony\\Bridge\\Doctrine\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Bridge/Doctrine',
        ),
        'Sonata\\DoctrineORMAdminBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sonata-project/doctrine-orm-admin-bundle',
        ),
        'Sonata\\DatagridBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sonata-project/datagrid-bundle',
        ),
        'Sonata\\CoreBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sonata-project/core-bundle',
        ),
        'Sonata\\Cache\\Tests\\' => 
        array (
            0 => __DIR__ . '/..' . '/sonata-project/cache/test',
        ),
        'Sonata\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/sonata-project/cache/lib',
        ),
        'Sonata\\BlockBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sonata-project/block-bundle',
        ),
        'Sonata\\AdminBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sonata-project/admin-bundle',
        ),
        'Sensio\\Bundle\\GeneratorBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sensio/generator-bundle',
        ),
        'Sensio\\Bundle\\FrameworkExtraBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sensio/framework-extra-bundle',
        ),
        'Sensio\\Bundle\\DistributionBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/sensio/distribution-bundle',
        ),
        'Ramsey\\Uuid\\' => 
        array (
            0 => __DIR__ . '/..' . '/ramsey/uuid/src',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Psr\\Http\\Message\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-message/src',
        ),
        'Psr\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/cache/src',
        ),
        'Oneup\\FlysystemBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/oneup/flysystem-bundle',
        ),
        'Nelmio\\SolariumBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/nelmio/solarium-bundle',
        ),
        'Negotiation\\' => 
        array (
            0 => __DIR__ . '/..' . '/willdurand/negotiation/src/Negotiation',
        ),
        'Monolog\\' => 
        array (
            0 => __DIR__ . '/..' . '/monolog/monolog/src/Monolog',
        ),
        'Minimalcode\\Search\\' => 
        array (
            0 => __DIR__ . '/..' . '/minimalcode/search/src/Minimalcode/Search',
        ),
        'League\\Flysystem\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/flysystem/src',
        ),
        'Knp\\Menu\\' => 
        array (
            0 => __DIR__ . '/..' . '/knplabs/knp-menu/src/Knp/Menu',
        ),
        'Knp\\Bundle\\PaginatorBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/knplabs/knp-paginator-bundle',
        ),
        'Knp\\Bundle\\MenuBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/knplabs/knp-menu-bundle',
        ),
        'Knp\\Bundle\\GaufretteBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/knplabs/knp-gaufrette-bundle',
        ),
        'Incenteev\\ParameterHandler\\' => 
        array (
            0 => __DIR__ . '/..' . '/incenteev/composer-parameter-handler',
        ),
        'Http\\Promise\\' => 
        array (
            0 => __DIR__ . '/..' . '/php-http/promise/src',
        ),
        'Http\\Client\\' => 
        array (
            0 => __DIR__ . '/..' . '/php-http/httplug/src',
        ),
        'Http\\Adapter\\Guzzle6\\' => 
        array (
            0 => __DIR__ . '/..' . '/php-http/guzzle6-adapter/src',
        ),
        'GuzzleHttp\\Psr7\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/psr7/src',
        ),
        'GuzzleHttp\\Promise\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/promises/src',
        ),
        'GuzzleHttp\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/guzzle/src',
        ),
        'FOS\\RestBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/friendsofsymfony/rest-bundle',
        ),
        'FOS\\HttpCache\\Tests\\' => 
        array (
            0 => __DIR__ . '/..' . '/friendsofsymfony/http-cache/tests',
        ),
        'FOS\\HttpCache\\' => 
        array (
            0 => __DIR__ . '/..' . '/friendsofsymfony/http-cache/src',
        ),
        'FOS\\HttpCacheBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/friendsofsymfony/http-cache-bundle',
        ),
        'FOS\\ElasticaBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/friendsofsymfony/elastica-bundle',
        ),
        'Exporter\\' => 
        array (
            0 => __DIR__ . '/..' . '/sonata-project/exporter/src',
        ),
        'Elastica\\' => 
        array (
            0 => __DIR__ . '/..' . '/ruflin/elastica/lib/Elastica',
        ),
        'Doctrine\\Instantiator\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/instantiator/src/Doctrine/Instantiator',
        ),
        'Doctrine\\Common\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/cache/lib/Doctrine/Common/Cache',
        ),
        'Doctrine\\Common\\Annotations\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/annotations/lib/Doctrine/Common/Annotations',
        ),
        'Doctrine\\Common\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/common/lib/Doctrine/Common',
        ),
        'Doctrine\\Bundle\\DoctrineCacheBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/doctrine-cache-bundle',
        ),
        'Doctrine\\Bundle\\DoctrineBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/doctrine-bundle',
        ),
        'Craue\\FormFlowBundle\\' => 
        array (
            0 => __DIR__ . '/..' . '/craue/formflow-bundle',
        ),
        'Cocur\\Slugify\\' => 
        array (
            0 => __DIR__ . '/..' . '/cocur/slugify/src',
        ),
    );

    public static $fallbackDirsPsr4 = array (
        0 => __DIR__ . '/../..' . '/src',
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_Extensions_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/extensions/lib',
            ),
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
        'S' => 
        array (
            'Solarium\\' => 
            array (
                0 => __DIR__ . '/..' . '/solarium/solarium/library',
            ),
            'SensioLabs\\Security' => 
            array (
                0 => __DIR__ . '/..' . '/sensiolabs/security-checker',
            ),
        ),
        'P' => 
        array (
            'PhpOption\\' => 
            array (
                0 => __DIR__ . '/..' . '/phpoption/phpoption/src',
            ),
            'PhpCollection' => 
            array (
                0 => __DIR__ . '/..' . '/phpcollection/phpcollection/src',
            ),
        ),
        'O' => 
        array (
            'Ornicar\\GravatarBundle\\' => 
            array (
                0 => __DIR__ . '/..' . '/ornicar/gravatar-bundle',
            ),
            'Oneup\\UploaderBundle' => 
            array (
                0 => __DIR__ . '/..' . '/oneup/uploader-bundle',
            ),
        ),
        'M' => 
        array (
            'Mopa\\Bundle\\BootstrapBundle\\' => 
            array (
                0 => __DIR__ . '/..' . '/mopa/bootstrap-bundle',
            ),
            'Mopa\\Bridge\\Composer' => 
            array (
                0 => __DIR__ . '/..' . '/mopa/composer-bridge/src',
            ),
            'Metadata\\' => 
            array (
                0 => __DIR__ . '/..' . '/jms/metadata/src',
            ),
        ),
        'K' => 
        array (
            'Knp\\Component' => 
            array (
                0 => __DIR__ . '/..' . '/knplabs/knp-components/src',
            ),
        ),
        'J' => 
        array (
            'JsonpCallbackValidator' => 
            array (
                0 => __DIR__ . '/..' . '/willdurand/jsonp-callback-validator/src',
            ),
            'JMS\\SerializerBundle' => 
            array (
                0 => __DIR__ . '/..' . '/jms/serializer-bundle',
            ),
            'JMS\\Serializer' => 
            array (
                0 => __DIR__ . '/..' . '/jms/serializer/src',
            ),
            'JMS\\' => 
            array (
                0 => __DIR__ . '/..' . '/jms/parser-lib/src',
            ),
        ),
        'G' => 
        array (
            'Guzzle\\Tests' => 
            array (
                0 => __DIR__ . '/..' . '/guzzle/guzzle/tests',
            ),
            'Guzzle' => 
            array (
                0 => __DIR__ . '/..' . '/guzzle/guzzle/src',
            ),
            'Genemu\\Bundle\\FormBundle' => 
            array (
                0 => __DIR__ . '/..' . '/genemu/form-bundle',
            ),
            'Gaufrette' => 
            array (
                0 => __DIR__ . '/..' . '/knplabs/gaufrette/src',
            ),
        ),
        'F' => 
        array (
            'FS\\SolrBundle' => 
            array (
                0 => __DIR__ . '/..' . '/floriansemm/solr-bundle',
            ),
            'FOS\\UserBundle' => 
            array (
                0 => __DIR__ . '/..' . '/friendsofsymfony/user-bundle',
            ),
        ),
        'D' => 
        array (
            'Doctrine\\ORM\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/orm/lib',
            ),
            'Doctrine\\DBAL\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/dbal/lib',
            ),
            'Doctrine\\Common\\Lexer\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/lexer/lib',
            ),
            'Doctrine\\Common\\Inflector\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/inflector/lib',
            ),
            'Doctrine\\Common\\Collections\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/collections/lib',
            ),
        ),
        'A' => 
        array (
            'Assetic' => 
            array (
                0 => __DIR__ . '/..' . '/kriswallsmith/assetic/src',
            ),
        ),
    );

    public static $classMap = array (
        'AppCache' => __DIR__ . '/../..' . '/app/AppCache.php',
        'AppKernel' => __DIR__ . '/../..' . '/app/AppKernel.php',
        'ArithmeticError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/ArithmeticError.php',
        'AssertionError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/AssertionError.php',
        'CallbackFilterIterator' => __DIR__ . '/..' . '/symfony/polyfill-php54/Resources/stubs/CallbackFilterIterator.php',
        'Collator' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/Collator.php',
        'DivisionByZeroError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/DivisionByZeroError.php',
        'Error' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/Error.php',
        'IntlDateFormatter' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/IntlDateFormatter.php',
        'Locale' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/Locale.php',
        'NumberFormatter' => __DIR__ . '/..' . '/symfony/symfony/src/Symfony/Component/Intl/Resources/stubs/NumberFormatter.php',
        'ParseError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/ParseError.php',
        'RecursiveCallbackFilterIterator' => __DIR__ . '/..' . '/symfony/polyfill-php54/Resources/stubs/RecursiveCallbackFilterIterator.php',
        'SessionHandlerInterface' => __DIR__ . '/..' . '/symfony/polyfill-php54/Resources/stubs/SessionHandlerInterface.php',
        'SqlFormatter' => __DIR__ . '/..' . '/jdorn/sql-formatter/lib/SqlFormatter.php',
        'TypeError' => __DIR__ . '/..' . '/symfony/polyfill-php70/Resources/stubs/TypeError.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb82f97cdf82207d6cccfcbd6127982e6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb82f97cdf82207d6cccfcbd6127982e6::$prefixDirsPsr4;
            $loader->fallbackDirsPsr4 = ComposerStaticInitb82f97cdf82207d6cccfcbd6127982e6::$fallbackDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitb82f97cdf82207d6cccfcbd6127982e6::$prefixesPsr0;
            $loader->classMap = ComposerStaticInitb82f97cdf82207d6cccfcbd6127982e6::$classMap;

        }, null, ClassLoader::class);
    }
}
