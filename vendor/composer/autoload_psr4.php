<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Polyfill\\Util\\' => array($vendorDir . '/symfony/polyfill-util'),
    'Symfony\\Polyfill\\Php70\\' => array($vendorDir . '/symfony/polyfill-php70'),
    'Symfony\\Polyfill\\Php56\\' => array($vendorDir . '/symfony/polyfill-php56'),
    'Symfony\\Polyfill\\Php55\\' => array($vendorDir . '/symfony/polyfill-php55'),
    'Symfony\\Polyfill\\Php54\\' => array($vendorDir . '/symfony/polyfill-php54'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Security\\Acl\\' => array($vendorDir . '/symfony/security-acl'),
    'Symfony\\Component\\ExpressionLanguage\\' => array($vendorDir . '/symfony/expression-language'),
    'Symfony\\Component\\Cache\\' => array($vendorDir . '/symfony/cache'),
    'Symfony\\Component\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Component'),
    'Symfony\\Bundle\\MonologBundle\\' => array($vendorDir . '/symfony/monolog-bundle'),
    'Symfony\\Bundle\\AsseticBundle\\' => array($vendorDir . '/symfony/assetic-bundle'),
    'Symfony\\Bundle\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bundle'),
    'Symfony\\Bridge\\Twig\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/Twig'),
    'Symfony\\Bridge\\Swiftmailer\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/Swiftmailer'),
    'Symfony\\Bridge\\ProxyManager\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/ProxyManager'),
    'Symfony\\Bridge\\PhpUnit\\' => array($vendorDir . '/symfony/phpunit-bridge'),
    'Symfony\\Bridge\\Monolog\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/Monolog'),
    'Symfony\\Bridge\\Doctrine\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/Doctrine'),
    'Sonata\\DoctrineORMAdminBundle\\' => array($vendorDir . '/sonata-project/doctrine-orm-admin-bundle'),
    'Sonata\\DatagridBundle\\' => array($vendorDir . '/sonata-project/datagrid-bundle'),
    'Sonata\\CoreBundle\\' => array($vendorDir . '/sonata-project/core-bundle'),
    'Sonata\\Cache\\Tests\\' => array($vendorDir . '/sonata-project/cache/test'),
    'Sonata\\Cache\\' => array($vendorDir . '/sonata-project/cache/lib'),
    'Sonata\\BlockBundle\\' => array($vendorDir . '/sonata-project/block-bundle'),
    'Sonata\\AdminBundle\\' => array($vendorDir . '/sonata-project/admin-bundle'),
    'Sensio\\Bundle\\GeneratorBundle\\' => array($vendorDir . '/sensio/generator-bundle'),
    'Sensio\\Bundle\\FrameworkExtraBundle\\' => array($vendorDir . '/sensio/framework-extra-bundle'),
    'Sensio\\Bundle\\DistributionBundle\\' => array($vendorDir . '/sensio/distribution-bundle'),
    'Ramsey\\Uuid\\' => array($vendorDir . '/ramsey/uuid/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'Oneup\\FlysystemBundle\\' => array($vendorDir . '/oneup/flysystem-bundle'),
    'Nelmio\\SolariumBundle\\' => array($vendorDir . '/nelmio/solarium-bundle'),
    'Negotiation\\' => array($vendorDir . '/willdurand/negotiation/src/Negotiation'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Minimalcode\\Search\\' => array($vendorDir . '/minimalcode/search/src/Minimalcode/Search'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'Knp\\Menu\\' => array($vendorDir . '/knplabs/knp-menu/src/Knp/Menu'),
    'Knp\\Bundle\\PaginatorBundle\\' => array($vendorDir . '/knplabs/knp-paginator-bundle'),
    'Knp\\Bundle\\MenuBundle\\' => array($vendorDir . '/knplabs/knp-menu-bundle'),
    'Knp\\Bundle\\GaufretteBundle\\' => array($vendorDir . '/knplabs/knp-gaufrette-bundle'),
    'Incenteev\\ParameterHandler\\' => array($vendorDir . '/incenteev/composer-parameter-handler'),
    'Http\\Promise\\' => array($vendorDir . '/php-http/promise/src'),
    'Http\\Client\\' => array($vendorDir . '/php-http/httplug/src'),
    'Http\\Adapter\\Guzzle6\\' => array($vendorDir . '/php-http/guzzle6-adapter/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'FOS\\RestBundle\\' => array($vendorDir . '/friendsofsymfony/rest-bundle'),
    'FOS\\HttpCache\\Tests\\' => array($vendorDir . '/friendsofsymfony/http-cache/tests'),
    'FOS\\HttpCache\\' => array($vendorDir . '/friendsofsymfony/http-cache/src'),
    'FOS\\HttpCacheBundle\\' => array($vendorDir . '/friendsofsymfony/http-cache-bundle'),
    'FOS\\ElasticaBundle\\' => array($vendorDir . '/friendsofsymfony/elastica-bundle'),
    'Exporter\\' => array($vendorDir . '/sonata-project/exporter/src'),
    'Elastica\\' => array($vendorDir . '/ruflin/elastica/lib/Elastica'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib/Doctrine/Common/Cache'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib/Doctrine/Common'),
    'Doctrine\\Bundle\\DoctrineCacheBundle\\' => array($vendorDir . '/doctrine/doctrine-cache-bundle'),
    'Doctrine\\Bundle\\DoctrineBundle\\' => array($vendorDir . '/doctrine/doctrine-bundle'),
    'Craue\\FormFlowBundle\\' => array($vendorDir . '/craue/formflow-bundle'),
    'Cocur\\Slugify\\' => array($vendorDir . '/cocur/slugify/src'),
    '' => array($baseDir . '/src'),
);
