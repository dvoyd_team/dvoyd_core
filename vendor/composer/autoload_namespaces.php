<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_Extensions_' => array($vendorDir . '/twig/extensions/lib'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Solarium\\' => array($vendorDir . '/solarium/solarium/library'),
    'SensioLabs\\Security' => array($vendorDir . '/sensiolabs/security-checker'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src'),
    'PhpCollection' => array($vendorDir . '/phpcollection/phpcollection/src'),
    'Ornicar\\GravatarBundle\\' => array($vendorDir . '/ornicar/gravatar-bundle'),
    'Oneup\\UploaderBundle' => array($vendorDir . '/oneup/uploader-bundle'),
    'Mopa\\Bundle\\BootstrapBundle\\' => array($vendorDir . '/mopa/bootstrap-bundle'),
    'Mopa\\Bridge\\Composer' => array($vendorDir . '/mopa/composer-bridge/src'),
    'Metadata\\' => array($vendorDir . '/jms/metadata/src'),
    'Knp\\Component' => array($vendorDir . '/knplabs/knp-components/src'),
    'JsonpCallbackValidator' => array($vendorDir . '/willdurand/jsonp-callback-validator/src'),
    'JMS\\SerializerBundle' => array($vendorDir . '/jms/serializer-bundle'),
    'JMS\\Serializer' => array($vendorDir . '/jms/serializer/src'),
    'JMS\\' => array($vendorDir . '/jms/parser-lib/src'),
    'Guzzle\\Tests' => array($vendorDir . '/guzzle/guzzle/tests'),
    'Guzzle' => array($vendorDir . '/guzzle/guzzle/src'),
    'Genemu\\Bundle\\FormBundle' => array($vendorDir . '/genemu/form-bundle'),
    'Gaufrette' => array($vendorDir . '/knplabs/gaufrette/src'),
    'FS\\SolrBundle' => array($vendorDir . '/floriansemm/solr-bundle'),
    'FOS\\UserBundle' => array($vendorDir . '/friendsofsymfony/user-bundle'),
    'Doctrine\\ORM\\' => array($vendorDir . '/doctrine/orm/lib'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Assetic' => array($vendorDir . '/kriswallsmith/assetic/src'),
);
