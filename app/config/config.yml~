imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: fr

framework:
    esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
 #   serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # handler_id set to null will use default session handler from php.ini
        handler_id:  ~
    fragments:       ~
    http_method_override: true

# SensioFrameworkBundle Configuration
sensio_framework_extra:
    request: { converters: true }
    view:
        annotations: true

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
# swiftmailer:
#     transport: "%mailer_transport%"
#     host:      "%mailer_host%"
#     username:  "%mailer_user%"
#     password:  "%mailer_password%"
#     spool:     { type: memory }


# JMSSerializerBundle Configuration
jms_serializer:
    enable_short_alias: true
    handlers:
        datetime:
            default_format: "c"
            default_timezone:  "UTC"
    property_naming:
        separator:   _
        lower_case:   true
    metadata:
        cache:  file
        debug:  "%kernel.debug%"
        file_cache:
            dir:   "%kernel.cache_dir%/serializer"
        auto_detection:   true

#  SonataAdminBunle  Configuration
#sonata_block:
#    default_contexts: [cms]
#    blocks:
#        sonata.admin.block.admin_list:
#            contexts:  [admin]


# FOSUserBundle Configuration
#fos_user:
#    db_driver:   orm
#    firewall_name:  main
#    user_class:  AppBundle\Entity\User


# AsseticBundle Configuration
assetic:
    debug:  '%kernel.debug%'
    use_controller: '%kernel.debug%'
    node: /usr/bin/node
    filters:
        cssrewrite: ~
        uglifyjs2:
            bin: '%kernel.root_dir%/Resources/node_modules/.bin/uglifyjs'
        uglifycss:
            bin:  '%kernel.root_dir%/Resources/node_modules/.bin/uglifycss'


# KnpMenuBundle Configuration
#knp_menu:
#    twig:
#        template: KnpMenuBundle::menu.html.twig
#    templating: false
#    default_renderer: twig

# MopaBoostrapBundle Configuration
#mopa_bootstrap:
 #   form: ~
 #   menu: ~

# OneUpLoaderBundle Configuration
#oneup_uploader:
#    chunks:
#        maxage:   604800
#        storage:
#            type:    gaufrette
#            directory:  %kernel.cache_dir%/dvoyd/uploader/chunks
#            filesystem:  gaufrette.gallery_filesystem
#            sync_buffer_size:  1M
#            stream_wrapper:    'gaufrette://gallery/'
#            prefix:            'chunks'
#        load_distribution:      true
#    orphanage:
#        maxage:    604800
#        directory:   ~
#    twig:    true
#    mappings:
#        gallery:
#            frontend:    blueimp
#            storage:
#                type:   gaufrette
#

# GauffretteBundle Configuration
#knp_gaufrette:
#    adapters:
#       gallery_adapter:
#            safe_local:
#                directory: %kernel.root_dir%/../dvoyd/gallery
#                create:   true
#    filesystems:
#        gallery:
#            adapter:    gallery_adapter

#    stream_wrapper: ~


# FosRestBundle Configuration
fos_rest:
    disable_csrf_role:    ROLE_API
    access_denied_listener:
        enabled:              false
        service:              null
        formats:
            name:  ~
    unauthorized_challenge:  null
    param_fetcher_listener:
        enabled:              true
        force:                true
        service:              null
    cache_dir:            '%kernel.cache_dir%/fos_rest'
    allowed_methods_listener:
        enabled:              false
        service:              null
    routing_loader:
        default_format:       json
        include_format:       true
    body_converter:
        enabled:              true
        validate:             false
        validation_errors_argument:  validationErrors
    service:
        router:               router
        templating:           templating
        serializer:           null
        view_handler:         fos_rest.view_handler.default
        inflector:            fos_rest.inflector.doctrine
        validator:            validator
    serializer:
        version:              null
        groups:               []
        serialize_null:       false
    view:
        default_engine:       twig
        force_redirects:
            html:             true
        mime_types:
            enabled:              false
            service:              null
            formats:
                jsonp:  'application/javascript+jsonp'
        formats:
            json:       true
            xml:        true
            rss:        false
        templating_formats:
            html:               true
        view_response_listener:
            enabled:              true
            force:                true
            service:              null
        failed_validation:    HTTP_BAD_REQUEST
        empty_content:        204
        serialize_null:       false
        jsonp_handler:
            callback_param:       callback
            mime_type:            application/javascript+jsonp
    body_listener:
        enabled:              true
        service:              app.hes_body_listen
        default_format:       html
        throw_exception_on_unsupported_content_type:  true
        decoders:
            json:       app.hes_decoder.json
            xml:        fos_rest.decoder.xml
        array_normalizer:
            service:              fos_rest.normalizer.camel_keys
            forms:                true
    format_listener:
        enabled:              true
        service:              fos_rest.format_listener
        rule:
            - { path: '^/solr' , priorities: ['json','xml'], fallback_format: 'json' }
    versioning:
        enabled: true
        default_version: ~
        resolvers:
            query:
                enabled: true
                parameter_name: version
            custom_header:
                enabled: true
                header_name: X-Accept-Version
            media_type:
                enabled: true
                regex: /(v|version)=(?P<version>[0-9\.]+)/
        guessing_order:
            - query
            - custom_header
            - media_type
#    exception:
#        enabled:    true
#        exception_controller: 'AppBundle\Controller\Api\HesExceptionController::showAction'
#        codes:
#            'Symfony\Component\Routing\Exception\ResourceNotFoundException': HTTP_NOT_FOUND
#            'Doctrine\ORM\OptimisticLockException': HTTP_CONFLICT
#        messages:
#            'AppBundle\Exception\MyExceptionWithASafeMessage': false

# NelmioSolariumBundle Configuration
nelmio_solarium:
    default_client:  AnNur
    endpoints:
        AnNur:
            dsn:   http://52.168.176.105:8983/solr/dvoyd_core_collection
            timeout:   5

    clients:
        AnNur:
            endpoints:     [AnNur]
            adapter_class: Solarium\Core\Client\Adapter\Curl


# KNPPaginatorBundle Configuration
knp_paginator:
    page_range: 4                      # default page range used in pagination control
    default_options:
        page_name: p                # page query parameter name
        sort_field_name: st          # sort field query parameter name
        sort_direction_name: dir # sort direction query parameter name
        distinct: true                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
        pagination: AppBundle:Pagination:pagination.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template
